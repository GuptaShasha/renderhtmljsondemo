import React, {Component} from 'react';
import {View,Text,Image,ScrollView} from 'react-native';
const data = require('./data.json');
const HEADING ={
  h1: 40,
  h2 : 20,
  h3 : 10
}
export default class App extends Component {
  renderComponent = (tagType,value,styleM='') => {
      if(styleM !== '') {
        try {
          var font_family = styleM.fontfamily;
          var font_size = styleM.fontsize;
          console.warn('size and family ', font_family, font_size);
        }
        catch(e) {
          console.warn("error ", e);
        }
      }
      var retVal = [];
      switch(tagType) {
        case 'h1' : 
                  retVal.push(<Text style={{fontSize : HEADING.h1}}>{value}</Text>);
                  break;
        case 'head' :
                  retVal.push(<Text>{value}</Text>);
                  break;
        case 'image' : 
                  retVal.push(<Image source={{uri : value}} style={{height : 300,width : 300}}/>);
                  break;
        case 'p' :
                  retVal.push(<Text style={{color : styleM.color,fontFamily : styleM.fontfamily,fontSize : styleM.fontsize}}>{value}</Text>)
      } 
      return retVal;
  }

  readJson = () => {
    var table = [];
    Object.values(data['data']).map(
        value => {
          if(value['status'] === true) {
              table.push(<Text>{value['title']}</Text>);
              if(Object.keys(value['sections']).length > 0) {
                Object.values(value['sections']).map(
                  section => {
                      if(Object.values(section['data']).length > 0) {
                          Object.values(section['data']).map(
                            section_data => {
                              if(Object.keys(section_data['children']).length > 0) {
                                Object.values(section_data['children']).map(
                                  section_data_item => {
                                    if(section_data_item['style'] !== undefined)
                                      table.push(this.renderComponent(section_data_item['tag'],section_data_item['html'],section_data_item['style']))
                                    else 
                                      table.push(this.renderComponent(section_data_item['tag'],section_data_item['html']))
                                  }
                                )
                              }
                            }
                          )
                      }
                  }
                )
              }
          }
        }
    );
    return table;
  }
  render() {
    return (
      <ScrollView scrollEnabled >
          {
            this.readJson()
          }
      </ScrollView>
    );
  }
}